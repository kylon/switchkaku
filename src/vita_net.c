#include "include/vita_net.h"

#include <psp2/io/fcntl.h>
#include <psp2/io/stat.h>
#include <psp2/sysmodule.h>
#include <psp2/net/http.h>
#include <psp2/net/net.h>
#include <psp2/net/netctl.h>

void netInit() {
	int size = 1*1024*1024;

    sceSysmoduleLoadModule(SCE_SYSMODULE_NET);

    SceNetInitParam netInitParam;

	netInitParam.memory = malloc(size);
    netInitParam.size = size;
    netInitParam.flags = 0;
    sceNetInit(&netInitParam);

    sceNetCtlInit();
}

void httpInit() {
    sceSysmoduleLoadModule(SCE_SYSMODULE_HTTP);

    sceHttpInit(1*1024*1024);
}

int download_file(const char *src, const char *dst) {
    int ret;

    int tpl = sceHttpCreateTemplate("henkaku offline", 2, 1);

    if (tpl < 0)
        return 0;

    int conn = sceHttpCreateConnectionWithURL(tpl, src, 0);

    if (conn < 0)
        return 0;

    int req = sceHttpCreateRequestWithURL(conn, 0, src, 0);

    if (req < 0)
        return 0;

    ret = sceHttpSendRequest(req, NULL, 0);

    if (ret < 0)
        return 0;

    unsigned char buf[4096] = {0};

    long long length = 0;
    ret = sceHttpGetResponseContentLength(req, &length);

    int fd = sceIoOpen(dst, SCE_O_WRONLY | SCE_O_CREAT | SCE_O_TRUNC, 6);
    int total_read = 0;

    if (fd < 0)
        return 0;

    while (1) {
        int read = sceHttpReadData(req, buf, sizeof(buf));

        if (read < 0)
            return 0;

        if (read == 0)
            break;

        ret = sceIoWrite(fd, buf, read);

        if (ret < 0 || ret != read) {
            if (ret < 0)
                return 0;

            return -1;
        }

        total_read += read;
    }

    ret = sceIoClose(fd);

    if (ret < 0)
        return 0;

    return 1;
}
