#pragma once

#define SCE_ERROR_ERRNO_ENOENT 0x80010002
#define SCE_ERROR_ERRNO_ENODEV 0x80010013

short exists(const char *path);
short touchFile(const char *path);
int isDir(const char *path);
short mkDir(const char *path, int mode);
short copyFile(const char *src, const char *dest);
short deleteFile(const char *path);
int rmDir(const char *path);
int copyDir(const char *src, const char *dest);
