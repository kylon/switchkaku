#pragma once

#include "common.h"

#include <psp2/ctrl.h>
#include <vita2d.h>

#include "sqlite3.h"

#define MAIL_DB        "ux0:email/message/mail.db"
#define EXPLOIT_HTML   "ux0:email/message/00/00/exploit.html"
#define HENKAKU_BIN    "ux0:picture/henkaku.bin"
#define TAI_SWTYPE     "ux0:/data/switchkaku/tai.swtype"
#define HENKAKU_SWTYPE "ux0:/data/switchkaku/henkaku.swtype"
#define SWITCHKAKU_LOG "ux0:/data/switchkaku/log.txt"

#define FAST_STRCMP(str1, str2) (str1[0] == str2[0] && !strcmp(str1, str2))
#define HOLD_END(oldPad, pad, control) ((oldPad.buttons & control) && (!(pad.buttons & control)))

static const char* tables[] = {
		"dt_folder",
		"dt_message_list",
		"dt_message_part",
		"dt_received_uid_list",
		"dt_task",
		"mt_account",
		NULL
};

struct commonRes {
	vita2d_pgf *pgf;
	char curExpl[20];
} resources;

int init();
int uninstall_exploit(const char *mode);
int install_exploit(const char *mode);
int sql_get_max(sqlite3 *db, const char *sql, int *id, FILE *log);
int remove_exploit_files(FILE *log);
int resetEmail();
int doSwitch(const char *selType, const char *curInst);
void draw_progress(const char *msg, int isError);
