#include "include/utils.h"
#include "include/file.h"
#include "include/vita_net.h"

#include <psp2/io/fcntl.h>

int init() {
	int ret;
	FILE *log;

	log = fopen(SWITCHKAKU_LOG, "w");
	memset(resources.curExpl, 0, 20);

	if (log == NULL)
		return 0;

	if (!exists(MAIL_DB)) {
		fprintf(log, "Creating empty database...\n");

		ret = resetEmail(log);

		if (!ret) {
			fprintf(log, "Failed!\n");
			fclose(log);

			return 0;
		}
	}

	if (exists(TAI_SWTYPE))
		snprintf(resources.curExpl, sizeof(resources.curExpl), "TaiHen");
	else if (exists(HENKAKU_SWTYPE))
		snprintf(resources.curExpl, sizeof(resources.curExpl), "Henkaku");
	else
		snprintf(resources.curExpl, sizeof(resources.curExpl), "Not Installed");

	if (!exists("ux0:/data/switchkaku")) {
		fprintf(log, "Creating switchkaku dirs...\n");

		ret = copyDir("ux0:/app/SWKK00001/data", "ux0:data");

		if (!ret) {
			fprintf(log, "Failed!\n");
			fclose(log);

			return 0;
		}
	}

	fclose(log);

	return 1;
}

int uninstall_exploit(const char *mode) {
	int ret;
	FILE *log;
	sqlite3 *db;
	sqlite3_stmt *stmt;
	char *accountName = exists(TAI_SWTYPE) ? "TaiHENkaku Offline":"HENkaku Offline";
	char sqlP0[250] = {0};

	log = fopen(SWITCHKAKU_LOG, "a+");

	if (log == NULL)
		return 0;

	draw_progress("Uninstall start...", 0);
	fprintf(log, "Uninstall start...");

	ret = sqlite3_open(MAIL_DB, &db);

	if (ret != SQLITE_OK) {
		fprintf(log, "Cannot open mail.db\n");
		sqlite3_close(db);
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	draw_progress("Deleting exploit email...", 0);
	sqlite3_snprintf(sizeof(sqlP0), sqlP0, "SELECT AccountID FROM mt_account WHERE AccountName = %Q", accountName);

	ret = sqlite3_prepare_v2(db, sqlP0, -1, &stmt, 0);

	if (ret != SQLITE_OK) {
		fprintf(log, "sql0 failed: %d\n", ret);
		sqlite3_close(db);
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	if (sqlite3_step(stmt) == SQLITE_ROW) {
		int aid = sqlite3_column_int(stmt, 0);

		for (const char **table = &tables[0]; *table; ++table) {
			char sql[256] = {0};
			char *error = NULL;

			snprintf(sql, sizeof(sql), "DELETE FROM %s WHERE AccountID = %d", *table, aid);
			ret = sqlite3_exec(db, sql, NULL, NULL, &error);

			if (ret != SQLITE_OK) {
				fprintf(log, "table sql failed: %s\n", error);
				sqlite3_free(error);
				sqlite3_finalize(stmt);
				sqlite3_close(db);
				fclose(log);
				draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

				return 0;
			}
		}
	}

	sqlite3_finalize(stmt);
	remove_exploit_files(log);
	sqlite3_close(db);
	fclose(log);

	return 1;
}

int install_exploit(const char *mode) {
	int ret, aid, fid, mid, muid, sid;
	sqlite3 *db;
	char sql[0x1000];
	char *error = NULL;
	FILE *log;
	const char *select_max_aid_sql = "SELECT MAX(AccountID) FROM mt_account";
	const char *select_max_fid_sql = "SELECT MAX(FolderUID) FROM dt_folder";
	const char *select_max_mid_sql = "SELECT MAX(MessageID) FROM dt_message_list";
	const char *select_max_muid_sql = "SELECT MAX(MessageUID) FROM dt_message_list";
	const char *select_max_setting_sql = "SELECT MAX(SettingID) FROM dt_setting";
	const char *mt_account_sql = "INSERT INTO mt_account (AccountID, Enable, Type, AccountName, Name, Address, HostName, Port, EncryptedUserID, EncryptedPassword, UseSSL, AuthType, ViewOrder, UnreadCount, PrimarySmtpID, LeaveMessageFlag, IMAPPathPrefix, UpdateDate) VALUES (%d, 1, 0, 'TaiHENkaku Offline', 'TaiHen', 'taihen@taihen.xyz', '127.0.0.1', 123, X'00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', X'00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', 0, 0, 0, 0, 0, 0, NULL, NULL)";
	const char *dt_folder_sql = "INSERT INTO dt_folder (AccountID, FolderID, FolderUID, FolderName, ParentFolderID, FolderPath, UnreadCount, UnreadCountDisplayFlag, Flag, MaxMessageUID, UpdateDate) VALUES(%d, -1, %d, 'EXPLOIT', 0, 'EXPLOIT', 0, 1, 0, NULL, NULL)";
	const char *dt_message_list_sql = "INSERT INTO dt_message_list (AccountID, FolderID, MessageID, MessageUID, MessageIDHeader, 'From', OriginalFrom, 'To', OriginalTo, Cc, OriginalCc, Bcc, OriginalBcc, ReplyTo, OriginalReplyTo, InReplyTo, 'References', Subject, OriginalSubject, SentDate, ReceiveDate, Priority, PreviewBody, AttachmentCount, Flag, DownloadedFlag, MessageSize, StatusFlag, ReplyMessageID, ForwardMessageID, Boundary) VALUES(%d, -1, %d, %d, '<taihen@taihen.xyz>', 'TaiHENkaku <taihen@taihen.xyz>', 'TaiHENkaku <taihen@taihen.xyz>', 'TaiHENkaku <taihen@taihen.xyz>', 'TaiHENkaku <taihen@taihen.xyz>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TaiHENkaku Offline', 'TaiHENkaku Offline', '2016-07-29 09:00:00', '2016-07-29 09:00:00', 3, '', 0, 1, 1, 0, 0, 0, 0, NULL)";
	const char *dt_message_part_sql = "INSERT INTO dt_message_part (AccountID, FolderID, MessageID, PartIndex, Type, OriginalHeader, MimeType, Charset, Encoding, FileName, CID, Section, FilePath, DownloadedFlag, StatusFlag) VALUES (%d, -1, %d, %d, %d, NULL, '%s', 'utf-8', '7BIT', NULL, NULL, %d, '%s', %d, 0)";
	const char *dt_setting_sql = "INSERT INTO dt_setting (SettingID, AutoFetch, Signature, BccMe, DefaultAccountID, NewMessageTemplate, ReplyMessageTemplate, ForwardMessageTemplate, BgReceivedFlag, UpdateDate) VALUES (1, NULL, NULL, NULL, -1, NULL, NULL, NULL, 0, NULL)";

	log = fopen(SWITCHKAKU_LOG, "a+");

	if (log == NULL) {
		return 0;
	}

	fprintf(log, "Install start..\n");
	draw_progress("Install start", 0);

	if (FAST_STRCMP(mode, "online")) {
		fprintf(log, "Online install!\n");
		netInit();
		httpInit();
	}

	draw_progress("Opening mail.db", 0);
	ret = sqlite3_open(MAIL_DB, &db);

	if (ret != SQLITE_OK) {
		fprintf(log, "Cannot open mail.db...retry...");
		ret = resetEmail(log);

		if (!ret) {
			fprintf(log, "Cannot reset email!\n");
			fclose(log);
			draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

			return 0;
		}

		ret = sqlite3_open(MAIL_DB, &db);

		if (ret != SQLITE_OK) {
			fprintf(log, "Cannot open mail.db: %d\n", ret);
			fclose(log);
			draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

			return 0;
		}
	}

	// make account: mt_account
	draw_progress("Creating account", 0);
	ret = sql_get_max(db, select_max_aid_sql, &aid, log);

	if (!ret) {
		fprintf(log, "max_aid failed: %d\n", ret);
		sqlite3_close(db);
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	snprintf(sql, sizeof(sql), mt_account_sql, aid);

	ret = sqlite3_exec(db, sql, NULL, NULL, &error);

	if (ret != SQLITE_OK) {
		fprintf(log, "mt_account failed: %d\n", ret);
		sqlite3_free(error);
		sqlite3_close(db);
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	// make folder: dt_folder
	ret = sql_get_max(db, select_max_fid_sql, &fid, log);

	if (!ret) {
		fprintf(log, "max_fid failed: %d\n", ret);
		sqlite3_close(db);
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	snprintf(sql, sizeof(sql), dt_folder_sql, aid, fid);

	ret = sqlite3_exec(db, sql, NULL, NULL, &error);

	if (ret != SQLITE_OK) {
		fprintf(log, "dt_folder failed: %d\n", ret);
		sqlite3_free(error);
		sqlite3_close(db);
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	// make message: dt_message_list
	draw_progress("Creating email", 0);
	ret = sql_get_max(db, select_max_mid_sql, &mid, log);

	if (!ret) {
		fprintf(log, "max_mid failed: %d\n", ret);
		sqlite3_close(db);
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	ret = sql_get_max(db, select_max_muid_sql, &muid, log);

	if (!ret) {
		fprintf(log, "max_muid failed: %d\n", ret);
		sqlite3_close(db);
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	snprintf(sql, sizeof(sql), dt_message_list_sql, aid, mid, muid);

	ret = sqlite3_exec(db, sql, NULL, NULL, &error);

	if (ret != SQLITE_OK) {
		fprintf(log, "message_list failed: %d\n", ret);
		sqlite3_free(error);
		sqlite3_close(db);
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	// make message: dt_message_part
	snprintf(sql, sizeof(sql), dt_message_part_sql, aid, mid, 1, 0, "TEXT/PLAIN", 1, "ux0:email/message/00/00/exploit.txt", 0);

	ret = sqlite3_exec(db, sql, NULL, NULL, &error);

	if (ret != SQLITE_OK) {
		fprintf(log, "message_part failed: %d\n", ret);
		sqlite3_free(error);
		sqlite3_close(db);
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	snprintf(sql, sizeof(sql), dt_message_part_sql, aid, mid, 2, 1, "TEXT/HTML", 2, EXPLOIT_HTML, 1);

	ret = sqlite3_exec(db, sql, NULL, NULL, &error);

	if (ret != SQLITE_OK) {
		fprintf(log, "message_part 2 failed: %d\n", ret);
		sqlite3_free(error);
		sqlite3_close(db);
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	// install dummy settings: dt_setting
	ret = sql_get_max(db, select_max_setting_sql, &sid, log);

	if (!ret) {
		fprintf(log, "max_setting failed: %d\n", ret);
		sqlite3_close(db);
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	if (sid == 1) {
		ret = sqlite3_exec(db, dt_setting_sql, NULL, NULL, &error);

		if (ret != SQLITE_OK) {
			fprintf(log, "dt_setting failed: %d\n", ret);
			sqlite3_free(error);
			sqlite3_close(db);
			fclose(log);
			draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

			return 0;
		}
	}

	sqlite3_close(db);

	// Create dirs
	draw_progress("Creating files", 0);
	ret = rmDir("ux0:/data/switchkaku/exploits/tai");

	if (!ret) {
		fprintf(log, "Cannot delete current exploit files!\n");
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	ret = mkDir("ux0:/email/message/00/00/", 0777);

	if (!ret) {
		fprintf(log, "Cannot create exploit folder!\n");
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	ret = mkDir("ux0:/picture", 0777);

	if (!ret) {
		fprintf(log, "Cannot create picture folder\n");
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	ret = mkDir("ux0:/data/switchkaku/exploits/tai", 0777);

	if (!ret) {
		fprintf(log, "Cannot create tai folder!\n");
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	if (FAST_STRCMP(mode, "online")) {
		draw_progress("Downloading lastest TaiHen", 0);
		const char *files[] = { "http://go.henkaku.xyz/offline/henkaku.bin",
		                        "http://go.henkaku.xyz/offline/exploit.html",
		                        "ux0:/data/switchkaku/exploits/tai/henkaku.bin",
		                        "ux0:/data/switchkaku/exploits/tai/exploit.html" };

		for (int i=0, j=2; i<2; i++, j++) {
			if (download_file(files[i], files[j]) < 0) {
				fprintf(log, "Download failed\n");
				fclose(log);
				draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

				return 0;
			}
		}
	}

	// Set Tai as default
	if (FAST_STRCMP(mode, "offline")) {
		ret = copyDir("ux0:/app/SWKK00001/data/switchkaku/exploits/tai", "ux0:/data/switchkaku/exploits/tai");

		if (!ret) {
			fprintf(log, "Cannot create files for offline install!\n");
			fclose(log);
			draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

			return 0;
		}
	}

	ret = copyFile("ux0:/data/switchkaku/exploits/tai/henkaku.bin", HENKAKU_BIN);

	if (!ret) {
		fprintf(log, "Cannot create henkaku.bin!\n");
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	ret = copyFile("ux0:/data/switchkaku/exploits/tai/exploit.html", EXPLOIT_HTML);

	if (!ret) {
		fprintf(log, "Cannot create exploit.html!\n");
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	ret = touchFile(TAI_SWTYPE);

	if (!ret) {
		fprintf(log, "Cannot create swtype file!\n");
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	snprintf(resources.curExpl, sizeof(resources.curExpl), "TaiHen");
	fclose(log);

	return 1;
}

int sql_get_max(sqlite3 *db, const char *sql, int *id, FILE *log) {
	int ret;
	sqlite3_stmt *stmt;

	ret = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);

	if (ret != SQLITE_OK) {
		fprintf(log, "sql_get_max failed: %d\n", ret);
		sqlite3_finalize(stmt);

		return 0;
	}

	if (sqlite3_step(stmt) == SQLITE_ROW)
		*id = sqlite3_column_int(stmt, 0) + 1;

	sqlite3_finalize(stmt);

	return 1;
}

int remove_exploit_files(FILE *log) {
	int ret;

	if (exists(TAI_SWTYPE))
		ret = deleteFile(TAI_SWTYPE);
	else if (exists(HENKAKU_SWTYPE))
		ret = deleteFile(HENKAKU_SWTYPE);
	else
		ret = 1;

	if (!ret) {
		fprintf(log, "Cannot delete swtype file\n");

		return 0;
	}

	if (exists(EXPLOIT_HTML)) {
		ret = deleteFile(EXPLOIT_HTML);

		if (!ret) {
			fprintf(log, "Cannot delete exploit html file\n");

			return 0;
		}
	}

	if (exists(HENKAKU_BIN)) {
		ret = deleteFile(HENKAKU_BIN);

		if (!ret) {
			fprintf(log, "Cannot delete exploit bin file\n");

			return 0;
		}
	}

	snprintf(resources.curExpl, sizeof(resources.curExpl), "Not Installed");

    return 1;
}

int resetEmail(FILE *log) {
	int ret;

	ret = remove_exploit_files(log);

	if (!ret)
		return 0;

	ret = deleteFile(MAIL_DB);

	if (!ret) {
		fprintf(log, "Cannot delete mail.db\n");

		return 0;
	}

	ret = copyFile("ux0:/data/switchkaku/exploits/mail.db", MAIL_DB);

	if (!ret) {
		fprintf(log, "Cannot create mail.db\n");

		return 0;
	}

	return 1;
}

int doSwitch(const char *selType, const char *curInst) {
	// Don t do heavy stuff if already set
	if (selType == curInst)
		return 1;

	char sql1[400] = {0};
	char sql2[300] = {0};
	sqlite3 *db;
	char *error = NULL;
	FILE *log;
	int ret;

	log = fopen(SWITCHKAKU_LOG, "a+");

	if (log == NULL)
		return 0;

	draw_progress("Switching exploit...", 0);
	ret = sqlite3_open(MAIL_DB, &db);

	if (ret != SQLITE_OK) {
		fprintf(log, "Cannot open mail.db\n");
		fclose(log);
		draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

		return 0;
	}

	ret = remove_exploit_files(log);

	if (!ret)
		goto exit;

	if (FAST_STRCMP(selType, "TaiHen")) {
		snprintf(sql1, sizeof(sql1),
		         "UPDATE dt_message_list SET MessageIDHeader = '<taihen@taihen.xyz>', 'From' = 'TaiHENkaku <taihen@taihen.xyz>', OriginalFrom = 'TaiHENkaku <taihen@taihen.xyz>', 'To' =  'TaiHENkaku <taihen@taihen.xyz>', OriginalTo = 'TaiHENkaku <taihen@taihen.xyz>', Subject = 'TaiHENkaku Offline', OriginalSubject = 'TaiHENkaku Offline' WHERE Subject = 'HENkaku Offline'");
		snprintf(sql2, sizeof(sql2),
		         "UPDATE mt_account SET AccountName = 'TaiHENkaku Offline', Name = 'TaiHen', Address = 'taihen@taihen.xyz' WHERE AccountName = 'HENkaku Offline'");

		ret = copyFile("ux0:/data/switchkaku/exploits/tai/henkaku.bin", HENKAKU_BIN);

		if (!ret) {
			fprintf(log, "Cannot create henkaku.bin\n");
			goto exit;
		}

		ret = copyFile("ux0:/data/switchkaku/exploits/tai/exploit.html", EXPLOIT_HTML);

		if (!ret) {
			fprintf(log, "Cannot create exploit.html\n");
			goto exit;
		}

		ret = deleteFile(HENKAKU_SWTYPE);

		if (!ret) {
			fprintf(log, "Cannot delete henkaku flag\n");
			goto exit;
		}

		ret = touchFile(TAI_SWTYPE);

		if (!ret) {
			fprintf(log, "Cannot create taihen flag\n");
			goto exit;
		}

		snprintf(resources.curExpl, sizeof(resources.curExpl), "TaiHen");
	} else {
		snprintf(sql1, sizeof(sql1),
		         "UPDATE dt_message_list SET MessageIDHeader = '<henkaku@henkaku.xyz>', 'From' = 'HENkaku <henkaku@henkaku.xyz>', OriginalFrom = 'HENkaku <henkaku@henkaku.xyz>', 'To' =  'HENkaku <henkaku@henkaku.xyz>', OriginalTo = 'HENkaku <henkaku@henkaku.xyz>', Subject = 'HENkaku Offline', OriginalSubject = 'HENkaku Offline' WHERE Subject = 'TaiHENkaku Offline'");
		snprintf(sql2, sizeof(sql2),
		         "UPDATE mt_account SET AccountName = 'HENkaku Offline', Name = 'Henkaku', Address = 'henkaku@henkaku.xyz' WHERE AccountName = 'TaiHENkaku Offline'");

		ret = copyFile("ux0:/data/switchkaku/exploits/henkaku/henkaku.bin", HENKAKU_BIN);

		if (!ret) {
			fprintf(log, "Cannot create henkaku.bin\n");
			goto exit;
		}

		ret = copyFile("ux0:/data/switchkaku/exploits/henkaku/exploit.html", EXPLOIT_HTML);

		if (!ret) {
			fprintf(log, "Cannot create exploit.html\n");
			goto exit;
		}

		ret = deleteFile(TAI_SWTYPE);

		if (!ret) {
			fprintf(log, "Cannot delete tai flag\n");
			goto exit;
		}

		ret = touchFile(HENKAKU_SWTYPE);

		if (!ret) {
			fprintf(log, "Cannot create henkaku flag\n");
			goto exit;
		}

		snprintf(resources.curExpl, sizeof(resources.curExpl), "Henkaku");
	}

	ret = sqlite3_exec(db, sql1, NULL, NULL, &error);

	if (ret != SQLITE_OK) {
		sqlite3_free(error);
		fprintf(log, "sql1 failed: %s\n", error);
		goto exit;
	}

	ret = sqlite3_exec(db, sql2, NULL, NULL, &error);

	if (ret != SQLITE_OK) {
		sqlite3_free(error);
		fprintf(log, "sql2 failed: %s\n", error);
		goto exit;
	}

	sqlite3_close(db);
	fclose(log);

	return 1;

exit:
	sqlite3_close(db);
	fclose(log);
	draw_progress("Error: see ux0:data/switchkaku/log.txt\n\nPress X to continue...", 1);

	return 0;
}

void draw_progress(const char *msg, int isError) {
	SceCtrlData pad;

	memset(&pad, 0, sizeof(pad));

	while (1) {
		sceCtrlPeekBufferPositive(0, &pad, 1);

		if (isError) {
			if (pad.buttons & SCE_CTRL_CROSS)
				break;
		}

		vita2d_start_drawing();
		vita2d_clear_screen();

		vita2d_pgf_draw_text(resources.pgf, 20, 30, RGBA8(220, 209, 4, 200), 1.1f, msg);

		vita2d_end_drawing();
		vita2d_swap_buffers();

		if (!isError)
			break;
	}

	return;
}
