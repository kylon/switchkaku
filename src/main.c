#include "include/utils.h"

#include <psp2/power.h>

void drawCredits() {
	SceCtrlData pad, oldPad;

	memset(&pad, 0, sizeof(pad));
	memset(&oldPad, 0, sizeof(oldPad));

	while (1) {
		sceCtrlPeekBufferPositive(0, &pad, 1);

		if (HOLD_END(oldPad, pad, SCE_CTRL_CIRCLE))
			break;

		oldPad = pad;

		vita2d_start_drawing();
		vita2d_clear_screen();

		vita2d_pgf_draw_text(resources.pgf, 20, 35, RGBA8(255, 255, 255, 200), 1.4f, "SwitchKaKu 2.0  -  by luck\n\n\n"
				"Credits:\n\n"
				"xyzz: OfflineInstaller\n"
				"Team Molecule: Henkaku & TaiHen\n"
				"marek256: BG Graphics\n"
				"kylon: SwitchKaku 2.0\n"
				"\n\n\n\n> Press ( ) to go back\n");

		vita2d_end_drawing();
		vita2d_swap_buffers();
	}

	return;
}

int main() {
    int ret;
	int isInstalled = 0;
	SceCtrlData pad, oldPad;

	vita2d_init();

	resources.pgf = vita2d_load_default_pgf();
	memset(&pad, 0, sizeof(pad));
	memset(&oldPad, 0, sizeof(oldPad));

	ret = init();

	if (!ret) {
		vita2d_fini();
		vita2d_free_pgf(resources.pgf);

		sceKernelExitProcess(0);
		return 0;
	}

	while (1) {
		sceCtrlPeekBufferPositive(0, &pad, 1);

		if (resources.curExpl[0] != 0 && resources.curExpl[0] != 'N')
			isInstalled = 1;

		if (HOLD_END(oldPad, pad, SCE_CTRL_TRIANGLE)) {
			if (!isInstalled) {
				ret = uninstall_exploit("online");

				if (ret)
					install_exploit("online");

				sceKernelDelayThread(150000);
			}
		} else if (HOLD_END(oldPad, pad, SCE_CTRL_CIRCLE)) {
			if (isInstalled) {
				ret = uninstall_exploit("uninstall");

				if (ret)
					isInstalled = 0;

				sceKernelDelayThread(300000);
			}
		} else if (HOLD_END(oldPad, pad, SCE_CTRL_CROSS)) {
			if (isInstalled) {
				doSwitch("TaiHen", resources.curExpl);
				sceKernelDelayThread(300000);
			}
		} else if (HOLD_END(oldPad, pad, SCE_CTRL_SQUARE)) {
			if (isInstalled) {
				doSwitch("Henkaku", resources.curExpl);
				sceKernelDelayThread(300000);
			}
		} else if (HOLD_END(oldPad, pad, SCE_CTRL_RTRIGGER)) {
			scePowerRequestColdReset();
			break;
		} else if (pad.buttons & SCE_CTRL_LTRIGGER) {
			drawCredits();
		} else if (HOLD_END(oldPad, pad, SCE_CTRL_SELECT)) {
			ret = resetEmail();

			if (ret)
				isInstalled = 0;

			sceKernelDelayThread(300000);
		} else if (HOLD_END(oldPad, pad, SCE_CTRL_START)) {
			if (!isInstalled) {
				ret = uninstall_exploit("offline");

				if (ret) {
					ret = install_exploit("offline");

					if (ret)
						isInstalled = 1;
				}
				sceKernelDelayThread(300000);
			}
		}

		oldPad = pad;

		vita2d_start_drawing();
		vita2d_clear_screen();

		vita2d_pgf_draw_text(resources.pgf, 20, 30, RGBA8(255, 255, 255, 200), 1.1f, "SwitchKaKu\n\n\n"
				"Install and manage the offline version of TaiHen and Henkaku.\n"
				"Back up your email data (ux0:/email) if you have anything important there.\n"
				"Make sure the Email app is closed, otherwise the installation may fail.");

		vita2d_pgf_draw_text(resources.pgf, 20, 170, RGBA8(255, 255, 255, 200), 1.1f, "Current Offline exploit: ");
		vita2d_pgf_draw_text(resources.pgf, 260, 170, RGBA8(220, 209, 4, 200), 1.1f, resources.curExpl);
		vita2d_pgf_draw_text(resources.pgf, 20, 220, RGBA8(255, 255, 255, 200), 1.1f, "#### Install Options ####");

		if (!isInstalled) {
			vita2d_pgf_draw_text(resources.pgf, 20, 250, RGBA8(255, 255, 255, 200), 1.1f, "> Press START to install TaiHen - [Offline]\n"
					"> Press TRIANGLE to install/update TaiHen - [Online]");
		} else {
			vita2d_pgf_draw_text(resources.pgf, 20, 260, RGBA8(255, 255, 255, 200), 1.1f, "> Press ( ) to uninstall");
		}

		vita2d_pgf_draw_text(resources.pgf, 20, 310, RGBA8(255, 255, 255, 200), 1.1f, "#### SwitchKaKu Options ####");

		if (isInstalled) {
			vita2d_pgf_draw_text(resources.pgf, 20, 350, RGBA8(255, 255, 255, 200), 1.1f, "> Press X to set TaiHen as default\n"
					"> Press [ ] to set Henkaku as default - ");
			vita2d_pgf_draw_text(resources.pgf, 420, 370, RGBA8(156, 38, 38, 200), 1.1f, "Deprecated");
		} else {
			vita2d_pgf_draw_text(resources.pgf, 20, 350, RGBA8(255, 255, 255, 200), 1.1f, "> No offline exploit found!");
		}

		vita2d_pgf_draw_text(resources.pgf, 20, 430, RGBA8(255, 255, 255, 200), 1.1f, "#### Extra Options ####\n\n"
				"> Press SELECT to reset your Email app\n"
				"> Press L to show credits\n"
				"> Press R to reboot\n");

		vita2d_end_drawing();
		vita2d_swap_buffers();
	}

	vita2d_fini();
	vita2d_free_pgf(resources.pgf);

	sceKernelExitProcess(0);
	return 0;
}
