#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <psp2/io/stat.h>
#include <psp2/io/fcntl.h>
#include <psp2/io/dirent.h>

#include "include/file.h"

short exists(const char *path) {
	SceIoStat stat = {0};
	int ret = sceIoGetstat(path, &stat);

	if (ret != SCE_ERROR_ERRNO_ENOENT && ret != SCE_ERROR_ERRNO_ENODEV)
		return 1;

	return 0;
}

short touchFile(const char *path) {
	int ret;

	ret = sceIoOpen(path, SCE_O_WRONLY | SCE_O_CREAT, 0777);
	sceIoClose(ret);

	return (ret >= 0);
}

int isDir(const char *path) {
	SceIoStat stat = {0};

	if (sceIoGetstat(path, &stat) < 0)
		return 0;

	return SCE_S_ISDIR(stat.st_mode);
}

short mkDir(const char *path, int mode) {

	if (isDir(path))
		return 1;

	int ret;
	int len = strlen(path);
	char p[len];

	memset(p, 0, len);

	for (int i=0; i<len; i++) {
		if (path[i] == '/') {
			if (strcmp(p, "ux0:/") == 0) {
				p[i] = path[i];
				continue;
			}

			if (!isDir(p)) {
				ret = sceIoMkdir(p, mode);

				if (ret < 0)
					return 0;

			}
		}
		p[i] = path[i];
	}

	ret = sceIoMkdir(path, mode);

	if (ret < 0)
		return 0;

	return 1;
}

short copyFile(const char *src, const char *dest) {
	// Is dest == src?
	if (!strcasecmp(src, dest) || !exists(src)) {
		return 0;
	}

	unsigned char buffer[128 * 128];
	int read = 0;
	int fsrc = sceIoOpen(src, SCE_O_RDONLY, 0777);
	int fdst  = sceIoOpen(dest, SCE_O_WRONLY | SCE_O_CREAT, 0777);

	if (fsrc < 0 || fdst < 0) {
		sceIoClose(fsrc);
		sceIoClose(fdst);

		return 0;
	}

	while ((read = sceIoRead(fsrc, buffer, sizeof(buffer))) > 0) {
		sceIoWrite(fdst, buffer, read);
	}

	sceIoClose(fsrc);
	sceIoClose(fdst);

	return 1;
}

int rmDir(const char *path) {
	SceUID dfd = sceIoDopen(path);

	if (dfd < 0) {
		int ret = sceIoRemove(path);

		if (ret < 0)
			return ret;
	}

	int res = 0;
	do {
		SceIoDirent dir;
		memset(&dir, 0, sizeof(SceIoDirent));

		res = sceIoDread(dfd, &dir);
		if (res > 0) {
			if (strcmp(dir.d_name, ".") == 0 || strcmp(dir.d_name, "..") == 0)
				continue;

			char *new_path = malloc(strlen(path) + strlen(dir.d_name) + 2);
			snprintf(new_path, 1024, "%s/%s", path, dir.d_name);

			if (SCE_S_ISDIR(dir.d_stat.st_mode)) {
				int ret = rmDir(new_path);

				if (ret <= 0) {
					free(new_path);
					sceIoDclose(dfd);

					return ret;
				}
			} else {
				int ret = sceIoRemove(new_path);

				if (ret < 0) {
					free(new_path);
					sceIoDclose(dfd);

					return ret;
				}
			}
			free(new_path);
		}
	} while (res > 0);

	sceIoDclose(dfd);

	int ret = sceIoRmdir(path);
	if (ret < 0)
		return ret;

	return 1;
}

short deleteFile(const char *path) {
	int ret;

	if (exists(path)) {
		ret = sceIoRemove(path);

		if (ret < 0)
			return 0;
	}

	return 1;
}

int copyDir(const char *src, const char *dest) {
	if (strcasecmp(src, dest) == 0)
		return 0;

	SceUID dfd = sceIoDopen(src);

	if (!isDir(dest)) {
		int ret = sceIoMkdir(dest, 0777);

		if (ret < 0) {
			sceIoDclose(dfd);

			return 0;
		}
	}

	int res = 0;

	do {
		SceIoDirent dir;
		memset(&dir, 0, sizeof(SceIoDirent));

		res = sceIoDread(dfd, &dir);
		if (res > 0) {
			if (strcmp(dir.d_name, ".") == 0 || strcmp(dir.d_name, "..") == 0)
				continue;

			char *new_src = malloc(strlen(src) + strlen(dir.d_name) + 2);
			snprintf(new_src, 1024, "%s/%s", src, dir.d_name);

			char *new_dest = malloc(strlen(dest) + strlen(dir.d_name) + 2);
			snprintf(new_dest, 1024, "%s/%s", dest, dir.d_name);

			int ret = 0;

			if (SCE_S_ISDIR(dir.d_stat.st_mode)) {
				ret = copyDir(new_src, new_dest);
			} else {
				ret = copyFile(new_src, new_dest);
			}

			free(new_dest);
			free(new_src);

			if (ret <= 0) {
				sceIoDclose(dfd);
				return ret;
			}
		}
	} while (res > 0);

	sceIoDclose(dfd);

	return 1;
}
